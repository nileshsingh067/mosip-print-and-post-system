package com.print.and.post.system.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.print.and.post.system.entiry.PpRequest;
import com.print.and.post.system.repo.PpRequestRepository;

@Component
public class PpRequestDAO {

	
	@Autowired 
	PpRequestRepository ppRequestRepo;
	
	
	public PpRequest addRequest(PpRequest request) {
		return ppRequestRepo.save(request);
	}
	
	public PpRequest getById(Long id) {
		return ppRequestRepo.findById(id).orElse(null);
	}

	public List<PpRequest> findByStatus(int status){
		return ppRequestRepo.findBystatus(status);
	}
}
