package com.print.and.post.system.notification.impl;

import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.print.and.post.system.entiry.PpRequest;
import com.print.and.post.system.notification.NotificationService;
import com.print.and.post.system.scheduled.JobScheduler;


@Service
public class SMSNotificationService implements NotificationService{
	private static final Logger logger=LoggerFactory.getLogger(SMSNotificationService.class);

	
	@Override
	public boolean sendNotification(PpRequest request, String subject, String message) {
		
		boolean flag=false;
		String loghead="sendNotification() To :"+request.getMobile()+" :: ";
		try {
		 RestTemplate restTemplate = new RestTemplate();
		        String resp=restTemplate.getForObject("http://api.msg91.com/api/v2/sendsms?country=91&sender=PPSYST&route=4&mobiles="+request.getMobile()+"&authkey=268449AOjrOOqpuNTz5c91c6af&message="+URLEncoder.encode(message),String.class);
		   logger.info(loghead+"SMS Sending response :: "+resp);
		flag=true;
		}catch(Exception e) {
			logger.error(loghead+"exception while sending SMS "+e);
		}
		return flag;
	}

}
