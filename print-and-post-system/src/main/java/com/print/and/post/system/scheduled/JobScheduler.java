package com.print.and.post.system.scheduled;

import java.util.List;

import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.print.and.post.core.data.PPData;
import com.print.and.post.system.controller.RequestReceiver;
import com.print.and.post.system.dao.PpRequestDAO;
import com.print.and.post.system.entiry.PpRequest;
import com.print.and.post.system.notification.impl.EmailNotificationService;
import com.print.and.post.system.notification.impl.SMSNotificationService;

@Component
public class JobScheduler {
	private static final Logger logger=LoggerFactory.getLogger(JobScheduler.class);
	
	@Autowired 
	private PpRequestDAO dao;
	@Autowired
	private EmailNotificationService emailNotificationService;
	@Autowired
	private SMSNotificationService smsNotificationService;
	
	
	@Autowired
    private Queue queue;

	
    @Autowired
    private JmsTemplate jmsTemplate;

	 @Scheduled(fixedRate = 60000)
	public void processReceivedData() {
		String logHead="processReceivedData():: ";
		logger.info(logHead+"Start Processing Received Data");
		List<PpRequest> requestList=dao.findByStatus(0);
		logger.info(logHead+"Found Count "+requestList.size());
		for(PpRequest newRequest:requestList) {
			//sent email to user ..//and send sms
			String subject="MOSIP || PRINT-AND-POST-SYSTEM || UIN-"+newRequest.getUin();
			String text="Hi "+newRequest.getName()+",\n\n\n\nYour request has been recieved for processing.we will notify you with further updates.\n\n\nOrder No : "+newRequest.getId()+"\nName : "+newRequest.getName()+"\n\n\n\nThanks,";
			//Template Your request for printing card has been recieved we will 
			emailNotificationService.sendNotification(newRequest, subject, text);
			smsNotificationService.sendNotification(newRequest, subject, text);
			try {
				jmsTemplate.convertAndSend(queue, convertData(newRequest));
			}catch(Exception e) {
				logger.error(logHead+"Exception "+e.getMessage());
			}
			newRequest.setStatus(1);
			dao.addRequest(newRequest);
		}
		 
	 }
	 @Scheduled(fixedRate = 80000)
		public void processOutForDelevirey() {
			String logHead="processOutForDelevirey():: ";
			logger.info(logHead+"Start Processing Received Data");
			List<PpRequest> requestList=dao.findByStatus(1);
			logger.info(logHead+"Found Count "+requestList.size());
			for(PpRequest newRequest:requestList) {
				//sent email to user ..//and send sms
				String subject="MOSIP || PRINT-AND-POST-SYSTEM || UIN-"+newRequest.getUin()+" || Out For Delivery";
				String text="Hi "+newRequest.getName()+",\n\n\n\nYour request has been processed and it is Out For Delivery.\n\n\nOrder No : "+newRequest.getId()+"\nName : "+newRequest.getName()+"\n\n\n\nThanks,";
				//Template Your request for printing card has been recieved we will 
				emailNotificationService.sendNotification(newRequest, subject, text);
				smsNotificationService.sendNotification(newRequest, subject, text);
				try {
					jmsTemplate.convertAndSend(queue, convertData(newRequest));
				}catch(Exception e) {
					logger.error(logHead+"Exception "+e.getMessage());
				}
				newRequest.setStatus(2);
				dao.addRequest(newRequest);
			}
			 
		 }
	 
	 @Scheduled(fixedRate = 100000)
		public void processDelivered() {
			String logHead="processDelivered():: ";
			logger.info(logHead+"Start Processing Received Data");
			List<PpRequest> requestList=dao.findByStatus(2);
			logger.info(logHead+"Found Count "+requestList.size());
			for(PpRequest newRequest:requestList) {
				//sent email to user ..//and send sms
				String subject="MOSIP || PRINT-AND-POST-SYSTEM || UIN-"+newRequest.getUin()+" || Delivered";
				String text="Hi "+newRequest.getName()+",\n\n\n\nYour request has been deliverd.\n\n\nOrder No : "+newRequest.getId()+"\nName : "+newRequest.getName()+"\n\n\n\nThanks,";
				//Template Your request for printing card has been recieved we will 
				emailNotificationService.sendNotification(newRequest, subject, text);
				smsNotificationService.sendNotification(newRequest, subject, text);
				try {
					jmsTemplate.convertAndSend(queue, convertData(newRequest));
				}catch(Exception e) {
					logger.error(logHead+"Exception "+e.getMessage());
				}
				newRequest.setStatus(3);
				dao.addRequest(newRequest);
			}
			 
		 }
	 public PPData convertData(PpRequest request) {
		 PPData data=new PPData();
		 data.setId(request.getId());
		 data.setUin(request.getUin());
		 data.setAddress(request.getAddress());
		 data.setDOB(request.getDOB());
		 data.setEmail(request.getEmail());
		 data.setMobile(request.getEmail());
		 data.setName(request.getEmail());
		 data.setStatus(request.getStatus());
		 data.setStatusMsg(request.getStatus()==0?"Request Received":request.getStatus()==1?"Out For Delivery":request.getStatus()==2?"Delivered":"UnDelivered");
		 
		 return data;
	 }
	
}
