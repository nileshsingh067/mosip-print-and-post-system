package com.print.and.post.system.notification.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.print.and.post.system.entiry.PpRequest;
import com.print.and.post.system.notification.NotificationService;

@Service
public class EmailNotificationService implements NotificationService{

	private static final Logger logger=LoggerFactory.getLogger(SMSNotificationService.class);
	private JavaMailSender javaMailSender;
	
	@Autowired
	public EmailNotificationService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
}
	
	
	@Override
	public boolean sendNotification(PpRequest request, String subject, String message) {
		// TODO Auto-generated method stub
		/*
		 * This JavaMailSender Interface is used to send Mail in Spring Boot. This
		 * JavaMailSender extends the MailSender Interface which contains send()
		 * function. SimpleMailMessage Object is required because send() function uses
		 * object of SimpleMailMessage as a Parameter
		 */
		boolean flag=false;
		String loghead="sendNotification() To :"+request.getEmail()+" :: ";
		try {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(request.getEmail());
		mail.setSubject(subject);
		mail.setText(message);

		/*
		 * This send() contains an Object of SimpleMailMessage as an Parameter
		 */
		javaMailSender.send(mail);
		flag=true;
		logger.info(loghead+"mail sent");
		}catch(Exception e) {
			logger.error(loghead+"exception while sending mail "+e);
		}
		return flag;
	}
	
}
