package com.print.and.post.system.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.print.and.post.system.entiry.PpRequest;

public interface PpRequestRepository extends CrudRepository<PpRequest, Long>{
	List<PpRequest> findBystatus (int status);
}
