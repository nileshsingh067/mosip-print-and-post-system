package com.print.and.post.system.notification;

import com.print.and.post.system.entiry.PpRequest;

public interface NotificationService {
	public boolean sendNotification(PpRequest request,String subject,String message);
}
