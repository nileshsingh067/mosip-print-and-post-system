package com.print.and.post.system.entiry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PpRequest {

	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	private Long uin;
	private String name;
	private String mobile;
	private String DOB;
	private String address;
	private int status;
	private String email;
	
	
	
	
	
	public PpRequest() {
		super();
	}
	
	

	public PpRequest(Long id, Long uin, String name, String mobile, String dOB, String address, int status,
			String email) {
		super();
		this.id = id;
		this.uin = uin;
		this.name = name;
		this.mobile = mobile;
		this.DOB = dOB;
		this.address = address;
		this.status = status;
		this.email = email;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUin() {
		return uin;
	}
	public void setUin(Long uin) {
		this.uin = uin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		this.DOB = dOB;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	@Override
	public String toString() {
		return "PpRequest [id=" + id + ", uin=" + uin + ", name=" + name + ", mobile=" + mobile + ", DOB=" + DOB
				+ ", address=" + address + ", status=" + status + ", email=" + email + "]";
	}
	
	
	
}
