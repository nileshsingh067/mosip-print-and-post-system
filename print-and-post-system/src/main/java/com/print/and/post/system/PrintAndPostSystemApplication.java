package com.print.and.post.system;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableJms
public class PrintAndPostSystemApplication {
	public static final String PRINT_AND_POST_RESPONSE_QUEUE="print-and-post-system-response-queue";
	public static void main(String[] args) {
		SpringApplication.run(PrintAndPostSystemApplication.class, args);
	}
	
}
