package com.print.and.post.system.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.print.and.post.system.dao.PpRequestDAO;
import com.print.and.post.system.entiry.PpRequest;



@RestController
@RequestMapping("/print-and-post/v1.0")
public class RequestReceiver {
	
	private static final Logger logger=LoggerFactory.getLogger(RequestReceiver.class);
	@Autowired
	private PpRequestDAO ppRequestDAO;
	

  
	
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String addrequest(@RequestBody PpRequest request) {
		request.setId(null);
		PpRequest ppResponse=ppRequestDAO.addRequest(request);
		return ""+ppResponse.getId();
	}
	
}
